#!/bin/bash
# execute temper-poll and re-format the output for track.C

if [ $# -ne 2 ]; then
  echo "error: need units and ndev; read script"
  exit
fi

units=$1 # 3 for C, 4 for F
ndev=$2

temper-poll | grep "Device #" > tmpfile_sh.tmp
outstr="$(date +%s)"

ndev=$(($ndev-1))

for d in `seq 0 ${ndev}`; do
  port_address=$(grep -E "^$d" device_map.dat | cut -d' ' -f3)
  temp=$(grep "#${d}" tmpfile_sh.tmp | cut -d' ' -f${units} | sed 's/.[C,F]//')
  if [ -z $temp ]; then temp="-1000"; fi
  #outstr="${outstr} ${port_address}__${temp}"
  outstr="${outstr} ${temp}"
done

echo $outstr
