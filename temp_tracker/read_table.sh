#!/bin/bash

if [ $# -eq 2 ]; then
  datafile="$1"
  address="$2"
else
  echo "usage: $0 [data file] [port address]"
  exit
fi

grep "(${address})" $datafile | grep -v "\-1000" > table_tmp.tmp

while read line; do 
  time=$(echo $line | awk '{print $1}')
  temp=$(echo $line | sed "s/^.*($address)=//g" | awk '{print $1}')
  echo "$time $temp"
done < table_tmp.tmp
