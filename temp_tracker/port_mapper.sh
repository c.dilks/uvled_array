#!/bin/bash
# maps USB ports and hubs, looking for thermometers
# currently uses `lsusb -t`; there may exist a better (faster) method
#
# note that `temper-poll -p` does not map full port for the hubs we are
# planning to use for run 17

#############################
# MAIN CONTROL VARIABLES
#############################

# which bus the highest-level USB port is on
bus=1

# name of thermometers
dev_name="Microdia TEMPer Temperature Sensor"
class_name="HID"

# if =1, print some extra info
debug=0

#############################
#############################

# if first argument is called, we use hardwire_map.info names
# instead of port-addresses when printing the final device_map.dat
if [ $# -eq 1 ]; then
  useHW=1
else
  useHW=0
fi


# probe connected usb devices with lsusb
lsusb -t > usb_tree.tmp

lsusb | grep -E "Bus.*${bus} Device" | grep "$dev_name" > usb_list.tmp

> num_list.tmp
tempnum_max=$(cat usb_list.tmp | wc -l)
let tempnum_max--
for i in `seq 0 $tempnum_max`; do
  echo $i >> num_list.tmp
done

paste -d' ' num_list.tmp usb_list.tmp > usb_list.tmp.num
mv usb_list.tmp{.num,}


# read tree of chosen bus
> bus_tree.tmp
IFS='' # preserve preceding indentations from usb tree
is_my_bus=0
while read line; do
  if [ $is_my_bus -eq 1 ]; then
    if [ -n "$(echo $line | grep -E "^/:  Bus")" ]; then
      is_my_bus=0
      break
    fi
  elif [ -n "$(echo $line | grep -E "^/:  Bus 0$bus")" ]; then
    is_my_bus=1
  fi
  if [ $is_my_bus -eq 1 ]; then
    echo $line >> bus_tree.tmp
  fi
done < usb_tree.tmp


# loop through tree
level_prev=0
num_thermos=0
while read branch; do

  # get device location
  if [ -n "$(echo $branch | grep -E "^/:")" ]; then continue; fi
  spaces="$(echo $branch | cut -d'|' -f1)"
  level=$(echo $(echo $spaces | grep -o " " | wc -l)/4 | bc)
  port=$(echo $branch | sed 's/^.* Port //;s/:.*$//g')
  
  # print device location
  if [ $debug -eq 1 ]; then
    echo "$branch"
    #echo "level=$level"
    #echo "port=$port"
  fi

  # convert device location into a "port address"
  if [ $level_prev -lt $level ]; then
    map[$level]=$port
    level_prev=$level
  else
    ll=$level_prev
    while [ $ll -gt $level ]; do
      map[$ll]=0
      let ll--
    done
    map[$level]=$port
  fi
  address=$(echo ${map[*]} | sed 's/0//g;s/  .*$//g;s/ $//;s/ /-/g')
  if [ $debug -eq 1 ]; then
    echo "${spaces}ADDRESS=${address}  LEVEL=${level}"
  fi


  # determine if this device is a TEMPer thermometer
  # NB: weird bug: driver name disappears after one temper-poll call, so
  # instead we use device class names to filter USB HIDs
  class="$(echo $branch | sed 's/^.*Class=//;s/,.*$//g')"
  if [ "$class" == "$class_name" ]; then
    # usb device number; used to map between `lsusb` and `lsusb -t`
    devnum="$(echo $branch | sed 's/^.* Dev //;s/,.*$//g')"

    devinfo="$(grep -E "Device.*${devnum}: ID" usb_list.tmp)" 

    # thermometer number used in temper-poll is determined by order in 
    # the `lsusb` command; see generation of `usb_list.tmp` file above
    tempnum=$(echo $devinfo | awk '{print $1}')
    
    # check if this device is a thermometer
    if [ -n "$devinfo" ]; then
      if [ -n "$(echo $devinfo | grep $dev_name)" ]; then
        interface=$(echo $branch | sed 's/^.* If //;s/,.*$//g')
        if [ $debug -eq 1 ]; then
          echo "${spaces}*---> Identified ${dev_name}"
        fi

        # thermometers are 2-interface devices; only 
        if [ $interface -eq 0 ]; then
          thermo_tempnum[$num_thermos]=$tempnum
          thermo_address[$num_thermos]=$address
          thermo_devnum[$num_thermos]=$devnum
          let num_thermos++
        fi
      fi
    fi
  fi

  #if [ $debug -eq 1 ]; then
    #echo "${spaces}devnum=$devnum"
    #echo "${spaces}driver=$driver"
    #echo "${spaces}class=$class"
    #echo "${spaces}interface=$interface"
  #fi

done < bus_tree.tmp


# load in hardwire map (for setting names of specific port addresses)
if [ $useHW -eq 1 ]; then
  for i in "${!thermo_address[@]}"; do
    hwname=$(grep ${thermo_address[$i]} hardwire_map.info | awk '{print $2}')
    if [ -n "$hwname" ]; then
      thermo_hwname[$i]="$hwname"
    else
      thermo_hwname[$i]="no_name"
    fi
  done
fi


# save device map, with columns [tempnum] [devnum] [portaddress] [hwname]
> device_map.tmp
for i in "${!thermo_tempnum[@]}"; do
  if [ $useHW -eq 1 ]; then
    echo "${thermo_tempnum[$i]} ${thermo_devnum[$i]} ${thermo_hwname[$i]}" >> device_map.tmp
  else
    echo "${thermo_tempnum[$i]} ${thermo_devnum[$i]} ${thermo_address[$i]}" >> device_map.tmp
  fi
done
cat device_map.tmp | sort -n > device_map.dat


# print device map
if [ $debug -eq 1 ]; then 
  echo -e "\nTHERMOMETERS\n---------------"
  cat device_map.dat
fi
